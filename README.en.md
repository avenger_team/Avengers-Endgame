# Avengers-Endgame

## 介绍

##### 复仇者集合!!!


| 姓名(身份) | 照片 | 背景 |
| -------- | -------- |  -------- | 
| CasterWx（钢铁侠） | ![](https://images.gitee.com/uploads/images/2019/0902/123544_13a293ce_4859429.jpeg) | 美国亿万富翁与军火制造商，以他独特的生活方式与其聪明才智及天才发明家，拥有赋予他超人力量，超人耐力，飞行能力与多种武器的动力装甲。 |
| ZhaoJQ（火箭浣熊） | ![](https://images.gitee.com/uploads/images/2019/0902/123544_97765f23_4859429.jpeg) | 一位被高度机械改造的智能拟人化浣熊， 尽管有时心不甘情不愿，但他仍然是银河护卫队中重要的一员。 | 
| XongQ（哈皮·霍根）| ![](https://images.gitee.com/uploads/images/2019/0902/123545_4181e5d6_4859429.jpeg) | 托尼·斯塔克的前司机/保镖，托尼成为钢铁侠后变成小辣椒的司机/保镖，后成为斯塔克公司的安全主管，不放过任何可疑的事情。 |
| ZhangJK（王） | ![](https://images.gitee.com/uploads/images/2019/0902/123545_c210f906_4859429.jpeg) | 奇异博士的忠实伙伴和助手，武术高手和魔法知识大师。在和钢铁侠. 奇异博士. 班纳博士一起在美国对抗了灭霸的手下后，他回去守卫魔法圣殿，后召集法师军团协助复仇者联盟对抗灭霸。 |


### 专辑曲目

1. 《Totally Fine》
2. 《Arrival》
3. 《No Trust》
4. 《Where Are They?》
5. 《Becoming Whole Again》
6. 《I Figured It Out》
7. 《Perfectly Not Confusing》
8. 《You Shouldn't Be Here》
9. 《The How Works》
10. 《Snap Out Of It》
11. 《So Many Stairs》
12. 《One Shot》
13. 《Watch Each Other's Six》
14. 《I Can't Risk This》
15. 《He Gave It Away》
16. 《The Tool of a Thief》
17. 《The Measure of a Hero》
18. 《Destiny Fulfilled》
19. 《In Plain Sight》
20. 《How Do I Look?》
21. 《Whatever It Takes》
22. 《Not Good》
23. 《Gotta Get Out》
24. 《I Was Made for This》
25. 《Tres Amigos》
26. 《Tunnel Scape》
27. 《Worth It》
28. 《Portals》
29. 《Get This Thing Started》
30. 《The One》
31. 《You Did Good》
32. 《The Real Hero》
33. 《Five Seconds》
34. 《Go Ahead》
35. 《Main on End》